package com.example.demo.model.response;

import com.example.demo.model.Match;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class MatchResponse {

    private Match match;
    private double odd;
    private String specifier;

}
