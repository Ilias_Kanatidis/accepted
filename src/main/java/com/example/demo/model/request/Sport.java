package com.example.demo.model.request;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum Sport {

    FOOTBALL,
    BASKETBALL

}
