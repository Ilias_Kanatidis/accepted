package com.example.demo.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@EqualsAndHashCode
public class CreateMatchRequest {
    private List<MatchRequest> requests;

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    @Builder
    @EqualsAndHashCode
    public static class MatchRequest {
        private String description;
        private String matchDate;
        private String matchTime;
        private String teamA;
        private String teamB;
        private Sport sport;
        private double odd;
        private String specifier;

    }
}
