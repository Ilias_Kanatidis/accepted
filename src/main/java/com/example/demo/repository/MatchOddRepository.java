package com.example.demo.repository;

import com.example.demo.model.MatchOdd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface to represent the connection with db for MatchOdd Entity
 */
@Repository
public interface MatchOddRepository extends JpaRepository<MatchOdd, Long> {

}
