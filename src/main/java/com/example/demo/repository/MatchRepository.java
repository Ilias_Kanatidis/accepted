package com.example.demo.repository;

import com.example.demo.model.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Interface to represent the connection with db for Match Entity
 */
@Repository
public interface MatchRepository extends JpaRepository<Match, Long> {

    List<Match> deleteByTeamAAndTeamB(String teamA, String teamB);

    Match findByTeamAAndTeamB(String teamA, String teamB);

    Match findByTeamAAndMatchDate(String teamA, Date date);

}
