package com.example.demo.controller;

import com.example.demo.model.request.CreateMatchRequest;
import com.example.demo.model.request.UpdateMatchRequest;
import com.example.demo.model.response.MatchResponse;
import com.example.demo.service.CreateMatchService;
import com.example.demo.service.DeleteMatchService;
import com.example.demo.service.GetMatchDetailsService;
import com.example.demo.service.UpdateMatchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Exposed apis for managing a match
 */
@SuppressWarnings("rawtypes")
@RestController
@RequiredArgsConstructor
@RequestMapping("/match")
public class MatchManagementController {

    private final CreateMatchService createMatchService;
    private final GetMatchDetailsService getMatchDetailsService;
    private final DeleteMatchService deleteMatchService;
    private final UpdateMatchService updateMatchService;

    @PostMapping(value = "/create",
            produces = APPLICATION_JSON_VALUE,
            consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity createMatchOdd(@RequestBody CreateMatchRequest request) throws Exception {

        List<MatchResponse> matches = createMatchService.perform(request);

        return ResponseEntity.status(HttpStatus.CREATED).body(matches);
    }

    @GetMapping(path = "/list")
    public ResponseEntity getMatches() {
        List<MatchResponse> response = getMatchDetailsService.getMatches();

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @GetMapping(path = "/one")
    public ResponseEntity findOne(@RequestParam(name = "teamA") String teamA,
                                  @RequestParam(name = "date") String date) {
        MatchResponse response = getMatchDetailsService.findOne(teamA, date);

        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @DeleteMapping(path = "/delete",
            produces = APPLICATION_JSON_VALUE,
            consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity deleteMatch(@RequestBody Map<String, Object> payload) throws Exception {

        List<MatchResponse> deletedMatch = deleteMatchService.perform(payload);

        return ResponseEntity.status(HttpStatus.OK).body(deletedMatch);
    }

    @PutMapping(path = "/update",
            produces = APPLICATION_JSON_VALUE,
            consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity updateMatch(@RequestBody UpdateMatchRequest request) {

        MatchResponse updatedMatch = updateMatchService.perform(request);

        return ResponseEntity.status(HttpStatus.OK).body(updatedMatch);
    }

}
