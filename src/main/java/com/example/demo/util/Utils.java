package com.example.demo.util;

import com.example.demo.model.Match;
import com.example.demo.model.response.MatchResponse;
import lombok.experimental.UtilityClass;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;


/**
 * A utility class that supports crud services
 */
@UtilityClass
public class Utils {

    public static String toString(Object o) {
        return o == null ? null : o.toString();
    }

    public static Date convertToDate(String date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date matchDate = null;
        try {
            matchDate = dateFormat.parse(date);
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        }
        return matchDate;
    }

    public static LocalDateTime convertToLocalDateTime(String time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(time, formatter);
    }

    public static MatchResponse buildFinalResponse(Match m) {
        return MatchResponse.builder()
                .match(m)
                .odd(m.getMatchOdd().getOdd())
                .specifier(m.getMatchOdd().getSpecifier())
                .build();
    }

}
