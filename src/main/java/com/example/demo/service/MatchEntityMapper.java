package com.example.demo.service;

import com.example.demo.model.Match;
import com.example.demo.model.MatchOdd;
import com.example.demo.repository.MatchRepository;
import com.example.demo.util.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.BiFunction;

import static com.example.demo.model.request.CreateMatchRequest.MatchRequest;

/**
 * Entity Mapper responsible to map request to the object #Match in order to be stored to db
 * and commands the actual save.
 */
@Component
@RequiredArgsConstructor
public class MatchEntityMapper implements BiFunction<List<MatchRequest>, List<Match>, List<Match>> {
    private final MatchRepository matchRepository;

    @Override
    public List<Match> apply(List<MatchRequest> requests, List<Match> newMatches) {
        requests.forEach(req -> {
            Match newMatch = Match.builder()
                    .matchDate(Utils.convertToDate(req.getMatchDate()))
                    .matchTime(Utils.convertToLocalDateTime(req.getMatchTime()))
                    .description(req.getDescription())
                    .sport(req.getSport())
                    .teamA(req.getTeamA())
                    .teamB(req.getTeamB())
                    .build();

            MatchOdd matchOdd = MatchOdd.builder()
                    .match(newMatch)
                    .odd(req.getOdd())
                    .specifier(req.getSpecifier())
                    .build();

            newMatch.setMatchOdd(matchOdd);
            newMatches.add(matchRepository.save(newMatch));

        });
        return newMatches;
    }
}
