package com.example.demo.service;

import com.example.demo.model.Match;
import com.example.demo.model.response.MatchResponse;
import com.example.demo.repository.MatchRepository;
import com.example.demo.util.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service to retrieve matches from db
 */
@RequiredArgsConstructor
@Service
public class GetMatchDetailsService {
    private final MatchRepository matchRepository;

    @Transactional(readOnly = true)
    public MatchResponse findOne(String teamA, String date) {

        return Optional.ofNullable(matchRepository.findByTeamAAndMatchDate(teamA, Utils.convertToDate(date)))
                .map(Utils::buildFinalResponse)
                .orElseGet(MatchResponse::new);
    }

    @Transactional(readOnly = true)
    public List<MatchResponse> getMatches() {

        List<Match> matches = matchRepository.findAll();
        if (CollectionUtils.isEmpty(matches)) {
            return Collections.emptyList();
        }

        return matches.stream()
                .map(Utils::buildFinalResponse)
                .collect(Collectors.toList());
    }
}
