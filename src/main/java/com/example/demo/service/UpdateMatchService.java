package com.example.demo.service;

import com.example.demo.model.Match;
import com.example.demo.model.request.UpdateMatchRequest;
import com.example.demo.model.response.MatchResponse;
import com.example.demo.repository.MatchRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service to update an already existing match
 */
@RequiredArgsConstructor
@Service
public class UpdateMatchService implements MatchManagementService<UpdateMatchRequest, MatchResponse> {
    private final MatchRepository matchRepository;

    @Transactional
    @Override
    public MatchResponse perform(UpdateMatchRequest request) {
        return Optional.ofNullable(matchRepository.findByTeamAAndTeamB(request.getTeamA(), request.getTeamB()))
                .map(m -> {
                    Match newMatch = createNewMatch(request, m);
                    return MatchResponse.builder()
                            .match(newMatch)
                            .specifier(newMatch.getMatchOdd().getSpecifier())
                            .odd(newMatch.getMatchOdd().getOdd())
                            .build();
                }).orElseGet(MatchResponse::new);
    }

    /**
     * Update the existing match
     */
    private Match createNewMatch(UpdateMatchRequest request, Match m) {
        m.setTeamA(request.getTeamA());
        m.setTeamB(request.getTeamB());
        m.getMatchOdd().setOdd(request.getOdd());
        m.getMatchOdd().setSpecifier(request.getSpecifier());
        return matchRepository.save(m);
    }
}
