package com.example.demo.service;

import com.example.demo.model.Match;
import com.example.demo.model.request.CreateMatchRequest;
import com.example.demo.model.response.MatchResponse;
import com.example.demo.repository.MatchOddRepository;
import com.example.demo.repository.MatchRepository;
import com.example.demo.util.Utils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.ValidationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.example.demo.model.request.CreateMatchRequest.MatchRequest;
import static com.example.demo.util.Utils.buildFinalResponse;

/**
 * Create Match Service
 */
@SuppressWarnings("ALL")
@RequiredArgsConstructor
@Service
public class CreateMatchService implements MatchManagementService<CreateMatchRequest, List<MatchResponse>> {
    private final MatchRepository matchRepository;
    private final MatchOddRepository matchOddRepository;
    private final MatchEntityMapper mapper;

    @Transactional
    @Override
    public List<MatchResponse> perform(CreateMatchRequest request) throws Exception {
        for (MatchRequest matchRequest : request.getRequests()) {
            validate(matchRequest);
        }

        List<MatchResponse> responses = new ArrayList<>();

        List<Match> matches = createMatchIfNotExists(request.getRequests());
        matches.forEach(m -> {
            matchOddRepository.save(m.getMatchOdd());
            MatchResponse matchResponse = buildFinalResponse(m);
            responses.add(matchResponse);
        });
        return responses;
    }


    /**
     * Validates every required attribute of request and in case of missing
     *
     * @throws ValidationException ValidationException
     */
    private void validate(MatchRequest request) throws ValidationException {
        List requests = Arrays.asList(request.getTeamA(), request.getTeamB(), request.getDescription(), request.getMatchDate(), request.getMatchTime(), request.getSpecifier(), request.getOdd());

        //TODO exception handling mechanism should be created in order to throw this.
        if (requests.stream().anyMatch(Objects::isNull)) throw new ValidationException("Invalid request");

    }

    /**
     * In case of a new match returns the match created
     * If the requested match already exists
     *
     * @throws ValidationException ValidationException
     */
    private List<Match> createMatchIfNotExists(List<MatchRequest> requests) throws ValidationException {

        boolean matchExists = requests.stream()
                .map(r -> matchRepository.findByTeamAAndMatchDate(r.getTeamA(), Utils.convertToDate(r.getMatchDate())))
                .anyMatch(Objects::nonNull);

        //TODO exception handling mechanism should be created in order to throw this.
        if (matchExists) throw new ValidationException("Cannot create match with specific team and date");

        List<Match> newMatches = new ArrayList<>();
        return createNewMatch(requests, newMatches);

    }

    /**
     * Creation of the new match
     */
    private List<Match> createNewMatch(List<MatchRequest> requests, List<Match> newMatches) {
        return mapper.apply(requests, newMatches);
    }

}
