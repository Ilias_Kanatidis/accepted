package com.example.demo.service;

/**
 * @param <T> the input
 * @param <R> the output
 */
public interface MatchManagementService<T, R> {

    R perform(T request) throws Exception;
}
