package com.example.demo.service;

import com.example.demo.model.Match;
import com.example.demo.model.response.MatchResponse;
import com.example.demo.repository.MatchRepository;
import com.example.demo.util.Utils;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.example.demo.util.Utils.buildFinalResponse;

/**
 * Delete Match Service
 */
@RequiredArgsConstructor
@Service
public class DeleteMatchService implements MatchManagementService<Map<String, Object>, List<MatchResponse>> {
    private final MatchRepository matchRepository;

    @Transactional
    @Override
    public List<MatchResponse> perform(Map<String, Object> request) throws Exception {
        try {
            List<MatchResponse> matchResponses = new ArrayList<>();
            List<Match> deletedMatches = matchRepository.deleteByTeamAAndTeamB(Utils.toString(request.get("teamA")), Utils.toString(request.get("teamB")));
            deletedMatches.forEach(m -> {
                MatchResponse matchResponse = buildFinalResponse(m);
                matchResponses.add(matchResponse);
            });
            return matchResponses;

        } catch (Exception e) {
            throw new Exception("failed to delete match");
        }
    }

}
