ACCEPTED
-----------

Comments:

1) As you will see, for similar operations, i have used various ways in order
technical reviewer to be able to form a better opinion on my technical knowledge.
   
2) Sorry for {docker-compose and flyway} -> time restriction. Well, before everything you should execute the following script.
   
3) Last and most important :) You could skip the business as i do not know much about odds on matches, bets etc. Now, concerning technical knowledge,
feel free to shoot on the head.

````

CREATE SCHEMA match;


CREATE TABLE match.match (
                       id bigint GENERATED ALWAYS AS IDENTITY NOT NULL,
                       description VARCHAR ( 50 ) NOT NULL,
                       match_date DATE NOT NULL,
                       match_time TIMESTAMP NOT NULL,
                       team_a VARCHAR ( 50 ) NOT NULL,
                       team_b VARCHAR ( 50 ) NOT NULL,
                       sport VARCHAR ( 15 ) NOT NULL,
                       primary key (id)
);

CREATE TABLE match.match_odd (
                            id bigint GENERATED ALWAYS AS IDENTITY NOT NULL,
                            specifier VARCHAR ( 50 ) NOT NULL,
                            odd NUMERIC NOT NULL,
                            match_id bigint,
                            primary key (id),
                            constraint match_id_fk foreign key (match_id) references match.match (id)
);

````


---------------------

````
## MatchManagementController.java


/match/create method = POST

1) Accepts a list of requests in order to create more than one matches.

IN PAYLOAD:


{
    "requests": [
    	{
    		"description": "olympiakos-pao",
    		"matchDate": "2021-05-05",
    		"matchTime": "2021-05-05 12:00:00",
    		"sport": "FOOTBALL",
    		"teamA": "olympiakos",
    		"teamB": "pao",
    		"specifier": "X",
    		"odd": 1.5 
    	}
    ]
}


----------------------------------------------------------------------------------------


2) Retuns specific match

/match/one method = GET

----Request params: 

teamA:olympiakos
date:2021-05-05

----------------------------------------------------------------------------------------


3) Retuns a list of created matches

/match/list method = GET

----------------------------------------------------------------------------------------


4) Deletes an already existing match based on the given teams.

/match/delete method = DELETE

IN PAYLOAD: 

{
    "teamA": "olympiakos",
    "teamB": "pao"
}


----------------------------------------------------------------------------------------


5) Updates a match based on the given request. Only the following attributes can be updated.

/match/update method = PUT


IN PAYLOAD: 

{
	"teamA": "olympiakos",
	"teamB": "pao",
	"specifier": "2",
	"odd": 13.1
}
````